<?php 
// Incluimos el archio transporte
include_once 'transporte.php';

// Clase creada por mí
class Bici extends transporte{

	// Atributo exclusivo de la clase moto
	private $numeroVelocidades;

	// Constructor
	public function __construct($nom, $vel, $com, $numeroVelocidades){
		// Se toma el método de la clase padre
		parent::__construct($nom, $vel, $com);
		// Se añade el atributo exclusivo de la clase
		$this->numeroVelocidades = $numeroVelocidades;
	}

	// Función para mostrar información de la moto
	public function resumenBici(){
		// Se toma la función crear ficha de la clase padre
		$mensaje=parent::crear_ficha();
		// Se agrega el atributo exclusivo de la clase
		$mensaje.='<tr>
		<td>Número de velocidades:</td>
		<td>'. $this->numeroVelocidades.'</td>				
		</tr>';
		// Se regresa el resumen
		return $mensaje;

	}

}