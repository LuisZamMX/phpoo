<?php  
include_once('Avion.php');
include_once('Barco.php');
include_once('Carro4.php');
include_once('Bici.php');

//declaracion de clase padre transporte
class transporte{
		//declaracion de atributos
	private $nombre;
	private $velocidad_max;
	private $tipo_combustible;

		//declaracion de metodo constructor
	public function __construct($nom,$vel,$com){
		$this->nombre=$nom;
		$this->velocidad_max=$vel;
		$this->tipo_combustible=$com;
	}

		//Este metodo genera un ficha en html
	public function crear_ficha(){
		$ficha='
		<tr>
		<td>Nombre:</td>
		<td>'. $this->nombre.'</td>				
		</tr>
		<tr>
		<td>Velocidad máxima:</td>
		<td>'. $this->velocidad_max.'</td>				
		</tr>
		<tr>
		<td>Tipo de combustible:</td>
		<td>'. $this->tipo_combustible.'</td>				
		</tr>';

		return $ficha;
	}

}

/*
Pasé este código a transporte, ya que no encuentro sentido dejarlo en el de alguna clase en particular. No sé si es mejor ponerlo en un archivo aparte o no es relevante ponerlo en cualquier clase, me gustaría que me pudieran retroalimentar :)
*/
$mensaje='';


if (!empty($_POST)){
	//declaracion de un operador switch
	switch ($_POST['tipo_transporte']) {
		case 'aereo':
			//creacion del objeto con sus respectivos parametros para el constructor
		$jet1= new avion('jet','400','gasoleo','2');
		$mensaje=$jet1->resumenAvion();
		break;
		case 'terrestre':
		$carro1= new carro('carro','200','gasolina','4');
		$mensaje=$carro1->resumenCarro();
		break;
		case 'maritimo':
		$bergantin1= new barco('bergantin','40','na','15');
		$mensaje=$bergantin1->resumenBarco();
		break;	
		case 'ecologico':
		$bergantin1= new Bici('Bicicleta','15','na','Monomarcha');
		$mensaje=$bergantin1->resumenBici();
		break;		
	}

}



