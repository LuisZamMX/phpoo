<?php  
//declaracion de clase token
class token{
		//declaracion de atributos
	private $nombre;
	private $token;
		//declaracion de metodo constructor
	public function __construct($nombre_front){
		$this->nombre=$nombre_front;
		$this->token= $this->crearContrasenia();
	}

		//declaracion del metodo mostrar para armar el mensaje con el nombre y token
	public function mostrar(){
		return 'Hola '.$this->nombre.' este es tu token: '.$this->token;
	}

		//declaracion de metodo destructor
	public function __destruct(){
			//destruye token
		$this->token='El token ha sido destruido';
		echo $this->token;
	}

	private function crearContrasenia() {

		$cadenaABC = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$longitudCadena = strlen($cadenaABC);
		$constrasenia = '';
		for ($i = 0; $i < 4; $i++) {
			$constrasenia .= $cadenaABC[rand(0, $longitudCadena - 1)];
		}
		return $constrasenia;
	} 

}

$mensaje='';


if (!empty($_POST)){
	//creacion de objeto de la clase
	$token1= new token($_POST['nombre']);
	$mensaje=$token1->mostrar();
}


?>
