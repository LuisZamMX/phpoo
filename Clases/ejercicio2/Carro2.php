<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	public $revision;

	//declaracion del método verificación
	public function verificacion($anioFabricacion){

		if ($anioFabricacion <= 1990){
			return 'No';
		}

		elseif($anioFabricacion <= 2010){
			return 'Revisión';
		}	

		elseif($anioFabricacion > 2010){
			return 'Sí';
		}

	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->revision=$Carro1->verificacion($_POST['anioFabricacion']);

}




