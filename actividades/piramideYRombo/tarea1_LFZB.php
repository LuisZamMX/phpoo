<?php
	
	//constante para el número máximo de filas
	define('MAXIMO_FILAS', 30);

	//Arreglo que va a contener los símbolos de cada renglón
	$aRenglones = [];

	//Símbolo que usaremos para formar la pirámide
	$cSimbolo = 'O';

	//Asignamos valor al primer elemento del arreglo
	$aRenglones[0] = $cSimbolo;

	//Asignamos valores a cada renglón
	for ($eContador = 1; $eContador < MAXIMO_FILAS ; $eContador++) { 
			
		$aRenglones[$eContador] = $aRenglones[$eContador - 1] . $cSimbolo;

	}

	//Ejercicio pirámide
	echo "<center><h1>Ejercicio pirámide</h1></center>";

		//Imprimimos renglones
		foreach ($aRenglones as $valor) {

			echo "<center> $valor </center> \n";

		}

	//Ejercicio rombo
	echo "<br><hr><center><h1>Ejercicio rombo</h1></center>";

		//Imprimimos la primer mitad del rombo
		foreach ($aRenglones as $valor) {

			echo "<center> $valor </center> \n";

		}

		//Imprimimos la segunda mitad del rombo
		for ($eContador = MAXIMO_FILAS - 1; $eContador >= 0; $eContador--) { 
			
			echo "<center> $aRenglones[$eContador] </center> \n";

		}

?>