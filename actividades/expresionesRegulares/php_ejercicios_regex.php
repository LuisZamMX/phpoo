<!doctype html>
<html lang="en" class="h-100">
<head>

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href=  "css/bootstrap.min.css">

  <!-- Mi estilo -->
  <link rel="stylesheet" href="css/estilo.css">

  <!-- Fuentes -->
  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab&display=swap" rel="stylesheet"> 

  <title>Expresiones Regulares</title>
  

</head>
<body class="d-flex flex-column h-100 body-login">

  <nav class="navbar navbar-dark bg-primary">
    <a class="navbar-brand" href="#">Curso PHP</a>
  </nav>

  <main>

    <div class="container">

      <h1 class="text-center">Expresiones Regulares</h1>

      <?php

      //Expresión regular para emails

      echo  "<h2>E-mail</h2>" .
      '<p>Expresión regular que detecta emails correctos</p>';

      $expresionCorreo = '/[A-Za-z0-9_-]+@([a-zA-Z0-9]+\.[A-Za-z])+/';
      $correoIncorrecto = 'JuanPerez@correo';
      $correoCorrecto = 'JuanPerez@correo.com.mx';

      echo "<p>Esta es la expresión:</p>" . 
      '<p class="font-italic text-success">' . $expresionCorreo . "</p>" .
            //Correo incorrecto:
      '<p>Cuando ingresamos el siguiente correo incorrecto: <span class="text-danger">' . $correoIncorrecto . "</span> Obtenemos la siguiente salida: " .
      preg_match($expresionCorreo, $correoIncorrecto) .
            //Correo correcto:
      '<p>Cuando ingresamos el siguiente correo correcto: <span class="text-success">' . $correoCorrecto . "</span> Obtenemos la siguiente salida: " .
      preg_match($expresionCorreo, $correoCorrecto);


      //Expresión regular para CURP

      echo  "<h2>CURP</h2>" .
      '<p>Expresión regular que detecta CURPS correctos</p>';

      $expresionCurp = '/^[A-Z]{4}[0-9]{6}[A-Z]{6}[0-9]{2}/';
      $curpIncorrecto = 'CURP23HDFMLS2305';
      $curpCorrecto = 'BABL310305HDFMLG04';

      echo "<p>Esta es la expresión:</p>" . 
      '<p class="font-italic text-success">' . $expresionCurp . "</p>" .
      //curp incorrecto:
      '<p>Cuando ingresamos el siguiente curp incorrecto: <span class="text-danger">' . $curpIncorrecto . "</span> Obtenemos la siguiente salida: " .
      preg_match($expresionCurp, $curpIncorrecto) .
      //curp correcto:
      '<p>Cuando ingresamos el siguiente curp correcto: <span class="text-success">' . $curpCorrecto . "</span> Obtenemos la siguiente salida: " .
      preg_match($expresionCurp, $curpCorrecto);


      //Expresión regular para símbolos especiales

      echo  "<h2>Símbolos especiales</h2>" .
      '<p>Expresión regular que detecta símbolos especiales</p>';

      $expresionSimbolo = '/[\.\*\+\-\^\$\?\[\]\{\}]+/';
      $simboloIncorrecto = 'Hola';
      $simboloCorrecto = '+./\{}';

      echo "<p>Esta es la expresión:</p>" . 
      '<p class="font-italic text-success">' . $expresionSimbolo . "</p>" .
      //simbolo Incorrecto:
      '<p>Cuando ingresamos la siguiente cadena incorrecta: <span class="text-danger">' . $simboloIncorrecto . "</span> Obtenemos la siguiente salida: " .
      preg_match($expresionSimbolo, $simboloIncorrecto) .
      //simbolo Correcto:
      '<p>Cuando ingresamos la siguiente cadena correcta: <span class="text-success">' . $simboloCorrecto . "</span> Obtenemos la siguiente salida: " .
      preg_match($expresionSimbolo, $simboloCorrecto);


      //Expresión regular para números decimales

      echo  "<h2>Números decimales</h2>" .
      '<p>Expresión regular que detecta números decimales</p>';

      $expresionNumero = '/[1-9]+\.[1-9]/';
      $numeroIncorrecto = '500';
      $numeroCorrecto = '15.25';

      echo "<p>Esta es la expresión:</p>" . 
      '<p class="font-italic text-success">' . $expresionNumero . "</p>" .
      //numero Incorrecto:
      '<p>Cuando ingresamos el siguiente número incorrecto: <span class="text-danger">' . $numeroIncorrecto . "</span> Obtenemos la siguiente salida: " .
      preg_match($expresionNumero, $numeroIncorrecto) .
      //numero Correcto:
      '<p>Cuando ingresamos el siguiente número correcto: <span class="text-success">' . $numeroCorrecto . "</span> Obtenemos la siguiente salida: " .
      preg_match($expresionNumero, $numeroCorrecto);

      ?>

    </div>

    <br><br>

  </main>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/app.js"></script>
</body>
</html>