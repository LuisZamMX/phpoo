<!doctype html>
<html lang="en" class="h-100">
<head>

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href=  "css/bootstrap.min.css">

  <!-- Mi estilo -->
  <link rel="stylesheet" href="css/estilo.css">

  <!-- Fuentes -->
  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab&display=swap" rel="stylesheet"> 

  <title>Lista</title>
  

</head>
<body class="d-flex flex-column h-100">

  <?php 
  $mensajeServidor = '';
  ?>

  <input type="text" class="form-control" value="<?php  echo $mensajeServidor; ?>" readonly>

  <!-- Contenido -->
  <main role="main" class="flex-shrink-0">

    <div class="container m-top">

      <h1>Lista de alumnos</h1>

      <a href="formulario.php" class="btn btn-success">Agregar nuevo alumno</a>

      <hr>
      
      <?php 
      include 'read.php';
      ?>

    </div>


  </main>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/app.js"></script>
</body>
</html>