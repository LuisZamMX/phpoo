<!doctype html>
<html lang="en" class="h-100">
<head>

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href=  "css/bootstrap.min.css">

  <!-- Mi estilo -->
  <link rel="stylesheet" href="css/estilo.css">

  <!-- Fuentes -->
  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab&display=swap" rel="stylesheet"> 

  <title>Login</title>
  

</head>
<body class="d-flex flex-column h-100 body-login">

  <div class="caja-login">
    <h1>Login</h1>

    <form>
      <br> 

      <!-- Número de cuenta -->
      <div class="form-group">
        <label for="numeroCuenta">Número de cuenta:</label>
        <input type="numeric" class="form-control" id="numeroCuenta" aria-describedby="emailHelp">
      </div>
      
      <!-- Contraseña -->
      <div class="form-group">
        <label for="contrasena">Contraseña:</label>
        <input type="password" class="form-control" id="contrasena">
      </div>
      
      <!-- Botón enviar -->
      <button type="submit" class="btn btn-primary">Entrar</button>
    </form>
  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/app.js"></script>
</body>
</html>