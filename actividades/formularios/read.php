<?php

$mensajeServidor = '';

try {
        require_once('conexion.php');

        // FETCH_ASSOC
        $stmt = $dbh->prepare("SELECT * FROM alumno");
        // Especificamos el fetch mode antes de llamar a fetch()
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        // Ejecutamos
        $stmt->execute();
        // Mostramos los resultados
        while ($row = $stmt->fetch()) {
            echo "Nombre: {$row['al_nombre']}";
            echo "Número de cuenta: {$row['al_numcta']} <br><br>";
        }

    } catch (Exception $e) {
        // Cualquier error lo imprimimos
        $mensajeServidor = $e->getMessage();
    } finally {
        // Cerramos la conexion a la base
        $dbh = null;
    }