<?php
include 'insert.php';
?>

<!doctype html>
<html lang="en" class="h-100">
<head>

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href=  "css/bootstrap.min.css">

  <!-- Mi estilo -->
  <link rel="stylesheet" href="css/estilo.css">

  <!-- Fuentes -->
  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab&display=swap" rel="stylesheet"> 

  <title>Formulario</title>
  

</head>
<body class="d-flex flex-column h-100">

  <input type="text" class="form-control" value="<?php  echo $mensajeServidor; ?>" readonly>

  <!-- Contenido -->
  <main role="main" class="flex-shrink-0">

    <div class="container m-top">

      <h1>Registro de alumno</h1>

      <hr>
      
      <!-- Formulario -->
      <form method="post">
        
        <!-- Número de cuenta -->
        <div class="form-group row">
          <label for="numeroCuenta" class="col-md-2 col-form-label">Número de cuenta:</label>
          <div class="col-md-10">
            <input type="numeric" class="form-control" name="numeroCuentaAlumno" id="numeroCuenta">
          </div>
        </div>
        
        <!-- Nombre -->
        <div class="form-group row">
          <label for="nombre" class="col-md-2 col-form-label">Nombre: </label>
          <div class="col-md-10">
            <input type="text" class="form-control" name="nombreAlumno" id="nombre">
          </div>
        </div>
        
        <!-- Primer apellido -->
        <div class="form-group row">
          <label for="primerApellido" class="col-md-2 col-form-label">Primer apellido:</label>
          <div class="col-md-10">
            <input type="text" class="form-control" name="primerApellidoAlumno" id="primerApellido">
          </div>
        </div>
        
        <!-- Segundo apellido  -->
        <div class="form-group row">
          <label for="segundoApellido" class="col-md-2 col-form-label">Segundo apellido:</label>
          <div class="col-md-10">
            <input type="text" class="form-control" name="segundoApellidoAlumno" id="segundoApellido">
          </div>
        </div>
        
        <!-- Contraseña -->
        <div class="form-group row">
          <label for="contrasena" class="col-md-2 col-form-label">Contraseña:</label>
          <div class="col-md-10">
            <input type="password " class="form-control" name="contrasenaAlumno" id="contrasena" disabled>
          </div>
        </div>
        
        <!-- Radios -->
        <fieldset class="form-group">
          <div class="row">
            <legend class="col-form-label col-sm-2 pt-0">Género</legend>
            <div class="col-sm-10">
              <div class="form-check">
                <input class="form-check-input" type="radio" name="generoAlumno" id="gridRadios1" value="Masculino" checked>
                <label class="form-check-label" for="gridRadios1">
                  M
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="generoAlumno" id="gridRadios2" value="Femenino">
                <label class="form-check-label" for="gridRadios2">
                  F
                </label>
              </div>
              <div class="form-check disabled">
                <input class="form-check-input" type="radio" name="generoAlumno" id="gridRadios3" value="Otro">
                <label class="form-check-label" for="gridRadios3">
                  Otro
                </label>
              </div>
            </div>
          </div>
        </fieldset>

        <!-- Fecha de nacimiento  -->
        <div class="form-group row">
          <label for="segundoApellido" class="col-md-2 col-form-label">Fecha de nacimiento:</label>
          <div class="col-md-10">
            <input type="date" class="form-control" name="fechaNacimientoAlumno" id="segundoApellido">
          </div>
        </div>
          
        <!-- Botón enviar -->
        <button type="submit" class="btn btn-primary">Enviar</button>
        <a href="lista.php" class="btn btn-secondary">Ver lista de alumnos</a>

      </form>

    </div>


  </main>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/app.js"></script>
</body>
</html>