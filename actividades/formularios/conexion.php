<?php

$host = "127.0.0.1";
$db = "dgtic_curso_sql";
$user = "root";
$password = "";

$dsn = "mysql:host=$host;dbname=$db";
$dbh = new PDO($dsn, $user, $password);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);