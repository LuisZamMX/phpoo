<?php

$mensajeServidor = '';

if (!empty($_POST)){

   try {
    // Preparamos la conexion a la base de datos
    require_once('conexion.php');
    // Insertamos datos
    $sql = "INSERT INTO alumno(al_numcta, al_nombre, al_apellido1, al_apellido2, al_genero, al_fechaNac) VALUES (?, ?, ?, ?, ?, ?)";
    // Datos 1: Parámetros posicion
    $stmt = $dbh->prepare($sql);

    $numeroCuenta = $_POST["numeroCuentaAlumno"];
    $nombre = $_POST["nombreAlumno"];
    $primerApellido = $_POST["primerApellidoAlumno"];
    $segundoApellido = $_POST["segundoApellidoAlumno"];
    $genero = $_POST["generoAlumno"];
    $fechaNacimiento = $_POST["fechaNacimientoAlumno"];

    $stmt->bindParam(1, $numeroCuenta);
    $stmt->bindParam(2, $nombre);
    $stmt->bindParam(3, $primerApellido);
    $stmt->bindParam(4, $segundoApellido);
    $stmt->bindParam(5, $genero);
    $stmt->bindParam(6, $fechaNacimiento);

    $mensajeServidor = ($stmt->execute()) ? "Se agrego a $nombre" : '';

} catch (Exception $e) {
    // Cualquier error lo imprimimos
    $mensajeServidor = $e->getMessage();
} finally {
    // Cerramos la conexion a la base
    $dbh = null;
} 

}

